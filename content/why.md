---
title: Why?
id: why
next: what-about
---
Traditionally, slides follow the limitations set by physical media.

In particular, they have a fixed size and aspect ratio.

I'm looking at you, Beamer.

If only there were a portable technology that was designed to be displayed on an arbitrary screen...
