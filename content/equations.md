---
title: some equations & code
id: equations
next: chart
layout: two-column
time: 100
---
We have someone's theorem $f$ [[Engel:2021ccn]]
```math
f(x) &= x^2 \\
     &= 2\int\D y y
```

---
```python
def f(x):
    return x**2 # square the number
```


