---
title: A TikZ chart
id: chart
next: two-column
time: 5
---

```tikz
[x=3cm,y=-2cm]

\node[block       ] (GD)  at (-1,-1) {\tt global\_def};
\node[block       ] (CLL) at ( 1,-1) {\tt collier};
\node[block       ] (FU)  at ( 0, 0) {\tt functions};
\node[block       ] (USR) at ( 1, 3) {\tt user};
\node[block       ] (PS)  at ( 0, 1) {\tt phase\_space};
\node[block       ] (ML)  at (-1, 1) {\tt \{pg\}\_mat\_el};
\node[block       ] (PG)  at (-1, 2) {\tt \{pg\}};
\node[block       ] (MG)  at (-1, 3) {\tt mat\_el};
\node[block       ] (INT) at (-1, 4) {\tt integrands};
\node[block       ] (VEG) at ( 1, 4) {\tt vegas};
\node[block       ] (XS)  at (-1, 5) {\tt mcmule};
\node[block       ] (TST) at ( 1, 5) {\tt test};

\draw [line       ] (CLL) --+ ( 0,0.5) -- (FU) ;
\draw [line       ] (GD)  --+ ( 0,0.5) -- (FU) ;
\draw [line       ] (FU)  --+ ( 1,0.5) -- (USR);
\draw [line       ] (FU)               -- (PS) ;
\draw [line       ] (FU)  --+ (-1,0.5) -- (ML) ;
\draw [line       ] (ML)               -- (PG) ;
\draw [line       ] (PG)               -- (MG) ;
\draw [line       ] (MG)               -- (INT);
\draw [line,dashed] (PS)               -- (PG)   node[pblock] {\tt ksoft};
\draw [line       ] (PS)  --+ (0,2)    -- (INT);
\draw [line       ] (USR)              -- (INT);
\draw [line,dashed] (USR)              -- (VEG) node[pblock] {\tt metadata};
\draw [line,dashed] (VEG)              -- (INT) node[pblock] {\tt bin\_it};
\draw [line       ] (INT)              -- (XS) ;
\draw [line       ] (VEG)              -- (XS) ;
\draw [line       ] (INT)              -- (TST);
\draw [line       ] (VEG)              -- (TST);

\draw [decorate,decoration={brace,amplitude=10pt}] ( -1.5,2.2) --
(-1.5,0.8);
```
