import transparencies.plugins
import logging

logger = logging.getLogger("transparencies.plugins." + __name__)


class timer(transparencies.plugins.global_action):
    def __init__(self, *args):
        super().__init__(*args)
        self.t = 0

    def preprocess_slide(self, slide):
        if "time" in slide:
            self.t += int(slide["time"])
            logger.info("Adding %ds, total now %s", int(slide["time"]), self.t)
        return slide

    def __del__(self):
        print("\nTalk will take %02d:%02d\n" % (
            self.t // 60,
            self.t % 60
        ))


main = timer
