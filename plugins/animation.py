import transparencies.plugins
import uuid
import re
import strictyaml
from strictyaml import Map, Optional, Str, Seq, Int, Bool, MapCombined

# the following is adapted from matplotlib's HTMLWriter
JS_INCLUDE = """
<link rel="stylesheet"
href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<script language="javascript">
function Animation(frames, img_id, slider_id, loop) {

    this.img_id = img_id;
    this.slider_id = slider_id;
    this.slider = document.getElementById(slider_id)
    this.img = document.getElementById(img_id)
    this.frames = frames;
    this.timer = null;
    this.frame = 0;
    this.loop = loop;

    for (var i=0; i<frames.length; i++) {
        this.frames[i].obj = new Image();
        this.frames[i].obj.src = frames[i].url;
    }
    this.slider.max = frames.length-1;

    this.set_frame(0);
}

Animation.prototype.set_frame = function(ind) {
    this.frame = ind;
    this.img.src = this.frames[ind].obj.src;
    this.slider.value = ind;
    this.play();
}

Animation.prototype.next_frame = function() {
    if(this.frame+1 < this.frames.length) {
        this.set_frame(this.frame+1);
    } else {
        if (this.loop) {
            this.set_frame(0);
        } else {
            this.timer = null;
        }
    }
}

Animation.prototype.pause = function() {
    if(this.timer) {
        clearTimeout(this.timer);
    }
    this.timer = null;
}

Animation.prototype.play = function() {
    if(this.frames[this.frame].speed > 0) {
        var t = this;
        this.timer = setTimeout(function(){t.next_frame();}, this.frames[this.frame].speed);
    } else {
        this.pause();
    }
}

Animation.prototype.click = function() {
    if(this.frames[this.frame].speed > 0) {
        if(this.timer) {
            this.pause();
        } else {
            this.play();
        }
    } else {
        this.next_frame();
    }
}
</script>
"""


# Style definitions for the HTML template
STYLE_INCLUDE = """
<style>
.animation {
    display: flex;
    text-align: center;
    position: relative;
    height: 100%;
    width: 100%;
    justify-content: center;
    align-items: flex-end;
    text-align: center;
    position: relative;
}
.animation img {
    max-height: 100%;
    max-width: 100%;
}
input[type=range].anim-slider {
    width: 300px;
    max-width: 30%;
    margin-left: auto;
    margin-right: auto;
}
.anim-controls {
    position: absolute;
    bottom: 0px;
    width: 100%;
    text-align: center;
}
.anim-controls button {
    padding: 0;
    width: 36px;
    height: 20.5px;
}
.anim-state label {
    margin-right: 8px;
}
.anim-state input {
    margin: 0;
    vertical-align: middle;
}
</style>
"""


# HTML template for HTMLWriter
DISPLAY_TEMPLATE = """
<div class="animation">
  <img id="_anim_img{id}" onclick="ani{id}.click()">
  <div class="anim-controls">
    <input id="_anim_slider{id}" type="range" class="anim-slider"
           name="points" min="0" max="1" step="1" value="0"
           oninput="ani{id}.set_frame(parseInt(this.value));">
      <button title="Pause" aria-label="Pause" onclick="ani{id}.pause()">
          <i class="fa fa-pause"></i></button>
      <button title="Play" aria-label="Play" onclick="ani{id}.play()">
          <i class="fa fa-play"></i></button>
      <button title="Next frame" aria-label="Next frame" onclick="ani{id}.next_frame()">
          <i class="fa fa-step-forward"></i></button>
  </div>
</div>


<script language="javascript">
    ani{id}=new Animation([{frames}],"_anim_img{id}","_anim_slider{id}", {loop_mode});
</script>
"""

class Range(strictyaml.ScalarValidator):
    def infinite_range(self, n=0):
        while True:
            yield n
            n = n + 1

    def validate_scalar(self, chunk):
        val = chunk.contents
        if strictyaml.utils.is_integer(val):
            return (int(val),)
        elif m := re.match("(\d+)\.\.(\d+)", val):
            return range(int(m.group(1)), int(m.group(2)))
        elif m := re.match("(\d+)\.\.=(\d+)", val):
            return range(int(m.group(1)), int(m.group(2))+1)
        elif m := re.match("\.\.(\d+)", val):
            return range(0, int(m.group(1)))
        elif m := re.match("\.\.=(\d+)", val):
            return range(0, int(m.group(1))+1)
        elif m := re.match("(\d+)\.\.", val):
            return self.infinite_range(int(m.group(1)))
        elif val == '..':
            return self.infinite_range()
        else:
            chunk.expecting_but_found("when expecting range")


_schema = Map({
    Optional("pattern"): Str(),
    Optional("nframes"): Int(),
    Optional("time"): MapCombined({}, Range(), Int()),

    Optional("list"): Seq(Map({
        "url": Str(), "time": Int(),
    })),

    Optional("loop", default=False): Bool(),
    Optional("fps", default=10): Int(),
})


class animation_renderer(transparencies.plugins.block_renderer):
    def can_render(self, token):
        return token.language == 'animation'

    def render_block_code(self, token):
        code = token.children[0].content
        config = strictyaml.load(code, _schema).data

        interval = 1000 // config['fps']

        if 'pattern' in config and 'nframes' in config:
            speeds = [1] * config['nframes']
            try:
                for spec, time in config['time'].items():
                    for f in spec:
                        if f < config['nframes']:
                            speeds[f] = time
                        else:
                            break
            except KeyError:
                pass

            pattern = config["pattern"]
            frames = ",".join(
                f'{{url: "{pattern % i}", speed: {speed * interval}}}'
                for i, speed in zip(range(config['nframes']), speeds)
            )
        elif "list" in config:
            frames = ",".join(
                f'{{url: "{i["url"]}", speed: {i["speed"] * interval}}}'
                for i in config['list']
            )

        return JS_INCLUDE + STYLE_INCLUDE + DISPLAY_TEMPLATE.format(
            id=uuid.uuid4().hex,
            Nframes=len(frames),
            frames=frames,
            loop_mode='true' if config['loop'] else 'false'
        )


main = animation_renderer
