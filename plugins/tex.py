import re
import subprocess
import tempfile
import pathlib
import jinja2
import transparencies.plugins
import logging

logger = logging.getLogger("transparencies.plugins." + __name__)

class LaTeXError(Exception):
    pass


class tex_renderer(transparencies.plugins.renderer):
    default_preamble = ""
    default_template = (
        r"\documentclass[preview]{standalone}"
        r"{{preamble}}"
        r"\begin{document}"
        r"{{body}}"
        r"\end{document}"
    )

    def __init__(self, head, opts):
        if "template" in opts:
            template_path = pathlib.Path(opts["template"]).resolve()
            env = jinja2.Environment(
                loader=jinja2.FileSystemLoader(template_path.parent)
            )
            self.template = env.get_template(template_path.name)
            logger.info("Loading template from %s", self.template.filename)
        else:
            if "preamble" in opts:
                if isinstance(opts["preamble"], list):
                    logger.info(
                        "Adding %d lines to the default preamble",
                        len(opts["preamble"])
                    )
                    opts["preamble"] = "\n".join(
                        [self.default_preamble] +
                        opts["preamble"]
                    )
                else:
                    logger.info("Loading preamble from %s", opts["preamble"])
                    with open(opts["preamble"]) as fp:
                        opts["preamble"] = fp.read()
            else:
                opts["preamble"] = self.default_preamble

            env = jinja2.Environment(loader=jinja2.BaseLoader())
            self.template = env.from_string(self.default_template)

        logger.debug("Preamble is %s", opts["preamble"])
        super().__init__(head, opts)

    def typeset(self, code, attr="", classes=[]):
        block = self.opts.copy()
        block["body"] = code
        latex = self.template.render(block)
        logger.debug("Running LaTeX as %s on", self.__class__.__name__)
        logger.debug(latex)

        with tempfile.TemporaryDirectory() as t:
            dir = pathlib.Path(t)

            with open(dir / "texfile.tex", "w") as fp:
                fp.write(latex)

            cmd = ["latex"] + self.opts.get("args", []) + ["texfile.tex"]
            logger.info("Executing %s in %s", " ".join(cmd), t)
            proc = subprocess.Popen(
                cmd, cwd=t,
                stdout=subprocess.PIPE, stdin=subprocess.PIPE
            )
            out, _ = proc.communicate()
            proc.wait()
            if proc.returncode != 0:
                logger.error(
                    "Executing %s in %s failed, file was",
                    " ".join(cmd), t
                )
                logger.error(code)
                logger.error(out.decode())
                raise LaTeXError
                return ""

            with open(dir/"texfile.dvi", "rb") as fp:
                dvi = fp.read()

            fs = re.findall(
                r"Preview: Fontsize (\d*)([a-z]*)",
                out.decode()
            )
            if fs:
                assert fs[0][1] == "pt"
                fs = int(fs[0][0])
                logger.debug("Font size %d pt", fs)
            else:
                fs = 10  # Hope for the best
                logger.warn("Couldn't estimate font size, using 10pt")

        cmd = ["dvisvgm", "-n", "--stdin", "--stdout", "--bbox=preview"]
        logger.debug("Executing %s", " ".join(cmd))
        proc = subprocess.Popen(
            cmd,
            stdin=subprocess.PIPE, stdout=subprocess.PIPE,
            stderr=subprocess.PIPE
        )
        svg, err = proc.communicate(dvi)
        svg = svg.decode('utf-8')
        proc.wait()
        if proc.returncode != 0:
            logger.error("Executing %s failed", " ".join(cmd))
            logger.error(err)
            return ""

        if not attr:
            classes.append("latex")
            attr = "class='%s' " % (" ".join(classes))

        whd = re.findall(r"width=([\.\d]*)pt, "
                         r"height=([\.\d]*)pt, "
                         r"depth=([\.\d]*)pt", err.decode())
        if whd:
            width = float(whd[0][0])
            height = float(whd[0][1])
            depth = float(whd[0][2])
            logger.debug(
                "Output size: w=%f, h=%f, d=%f",
                width, height, depth
            )

            # TODO
            attr += "style='height: %fem; vertical-align: -%fem;'" % (
                (height + depth) / fs,
                depth / fs
            )
        else:
            logger.warn("Couldn't estimate height and depth, assuming (1em,0em)")

        svg = re.sub(
            r"width='[\d\.]*pt' height='[\d\.]*pt'",
            attr,
            svg
        )
        return svg
