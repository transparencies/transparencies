import transparencies.plugins
import pygments
import pygments.formatters.html
import pygments.lexers
import logging

logger = logging.getLogger("transparencies.plugins." + __name__)


class pygments_renderer(transparencies.plugins.block_renderer):
    def __init__(self, head, opts):
        super().__init__(head, opts)
        self.style=opts.get("style", "default")
        logger.info("Syntax logger with style %s", self.style)
        self.formatter = pygments.formatters.html.HtmlFormatter(
            style=self.style
        )

    def can_render(self, token):
        lexers = [
            i
            for _, _, i, _, _ in pygments.lexers.LEXERS.values()
            if token.language in i
        ]
        logger.debug(
            "Found %d/%d lexers",
            len(lexers), len(pygments.lexers.LEXERS)
        )
        if len(lexers) > 0:
            return True
        else:
            return False

    def render_block_code(self, token):
        if not token.language:
            return super().render_block_code(token)
        code = token.children[0].content
        lexer = pygments.lexers.get_lexer_by_name(token.language)
        return pygments.highlight(
            code,
            lexer,
            self.formatter
        )

    def prefix(self):
        return (
            "<style type=\"text/css\">"
            + self.formatter.get_style_defs()
            + "</style>"
        )


main = pygments_renderer
