import re
import transparencies.plugins
from tex import tex_renderer


class tikzblock_renderer(transparencies.plugins.block_renderer, tex_renderer):
    default_preamble = "\\usepackage{tikz}"
    default_template = (
        r"\documentclass[tikz,preview]{standalone}"
        r"{{preamble}}"
        r"\begin{document}"
        r"\begin{tikzpicture}"
        r"{{body}}"
        r"\end{tikzpicture}"
        r"\end{document}"
    )

    def can_render(self, token):
        return token.language == 'tikz'

    def render_block_code(self, token):
        code = token.children[0].content
        fn = self.write(self.typeset(code, classes=["tikz"]), ".svg")

        return (
            "<div class=\"image\" style=\"background-image: url(%s);\"></div>"
        ) % fn


main = tikzblock_renderer
