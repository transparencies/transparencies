import re
import mistletoe.span_token
import transparencies.plugins
from tex import tex_renderer


class Maths(mistletoe.span_token.SpanToken):
    pattern = re.compile(r'(\${1,2})([^$]+?)\1')
    parse_inner = False
    parse_group = 0


class texinline_renderer(transparencies.plugins.token_renderer, tex_renderer):
    default_preamble = "\\usepackage{amsmath}\\usepackage{amssymb}"
    default_template = (
        r"\documentclass[preview]{standalone}"
        r"{{preamble}}"
        r"\begin{document}"
        r"$"
        r"{{body}}"
        r"$"
        r"\end{document}"
    )

    def render_maths(self, token):
        code = token.pattern.match(token.content).groups()[1]
        return self.typeset(code, classes=["maths", "inline"])


class inline_token(transparencies.plugins.token):
    token = Maths
    renderers = [texinline_renderer]


main = inline_token
