import re
import mistletoe.span_token
import transparencies.plugins
import requests
import time


class MetadataFetch:
    def __init__(self, key, opts):
        self.opts = opts
        self.key = key
        self.authors = []
        self.title = ""
        self.year = ""
        self.month = ""
        self.url = ""
        self.identifier = ""
        self.fetch()

    def fetch(self):
        pass


class arXivFetch(MetadataFetch):
    pass


class inspireFetch(MetadataFetch):
    def fetch(self):
        time.sleep(0.25)  # rate limit: 4/s
        r = requests.get(
            f"https://inspirehep.net/api/literature?q=texkey%3D{self.key}"
        )
        assert r.ok
        j = r.json()["hits"]
        assert j["total"] == 1
        j = j["hits"][0]["metadata"]

        def parse_name(rec):
            try:
                return (rec["first_name"], rec["last_name"])
            except KeyError:
                return rec["full_name"]

        self.authors = [
            parse_name(i) for i in j["authors"]
        ]
        self.title = j["titles"][0]["title"]

        if "legacy_creation_date" in j:
            d = j["legacy_creation_date"]
        elif "earliest_date" in j:
            d = j["earliest_date"]
        else:
            raise KeyError("Couldn't find date")
        self.year, self.month = [
            int(i) for i in d.split("-")[:2]
        ]

        if "arxiv_eprints" in j:
            self.url = "https://arxiv.org/abs/" + j["arxiv_eprints"][0]["value"]
        elif "doi" in j:
            self.url = "https://doi.org/" + j["dois"][0]["value"]
        else:
            raise KeyError("Couldn't find url")


class localFetch(MetadataFetch):
    def fetch(self):
        key = self.key.split(':')[1]
        entry = self.opts['local'][key]

        self.authors = [i.split('_') for i in entry['authors']]
        self.year = entry['year']

        if 'title' in entry:
            self.title = entry['title']
        if 'url' in entry:
            self.url = entry['url']


class Citation(mistletoe.span_token.SpanToken):
    patterns = {
        "arXivNew": (r"\d{4}\.\d{4,}", arXivFetch),
        "arXivOld": (r"[a-zA-Z\.\-]*/\d{7}", arXivFetch),
        "local": ("local:[a-zA-Z\d]*", localFetch),
        "inspire": (r"[A-Z][A-Za-z\d\-]*:\d\d\d\d[a-z]{2,3}", inspireFetch)
    }

    pattern = re.compile(
        r"\[\[(" + "|".join(
            f"(?P<{key}>{value})"
            for key, (value, _) in patterns.items()
        ) + r")\]\]")

    parse_inner = False
    parse_group = 0

    def __init__(self, match):
        type, key = next(filter(
            lambda a: a[1],
            zip(self.patterns.keys(), match.groups()[1:])
        ))
        _, c = self.patterns[type]
        self.metadata = c(key, self.opts)


class citation_renderer(transparencies.plugins.token_renderer):
    def render_citation(self, token):
        key = []
        own_paper = False
        for [f, l] in token.metadata.authors:
            if [f,l] in self.opts["me"]:
                own_paper = True
                continue
            key.append(l)

        if own_paper:
            key.append(self.opts["initial"])

        key = f"[{', '.join(key)} {token.metadata.year}]"
        return f"<a class=\"reference\" href=\"{token.metadata.url}\">{key}</a>"


class citation_token(transparencies.plugins.token):
    token = Citation
    renderers = [citation_renderer]


main = citation_token
