import setuptools
import os


def get_data_files(base):
    for (dirpath, dirnames, filenames) in os.walk(base):
        if filenames:
            yield (
                os.path.join("share", "transparencies", dirpath),
                [
                    os.path.join(dirpath, filename)
                    for filename in filenames
                ]
            )


this_directory = os.path.abspath(os.path.dirname(__file__))
with open(os.path.join(this_directory, 'README.md')) as f:
    long_description = f.read()

setuptools.setup(
    name='transparencies',  # Replace with your own username
    version='0.2.2',
    license='AGPLv3',
    description='A python slide generator',
    long_description=long_description,
    long_description_content_type='text/markdown',
    packages=setuptools.find_packages(),
    classifiers=[
        'License :: OSI Approved :: GNU Affero General Public License v3',
        'Programming Language :: Python :: 3',
        'Operating System :: OS Independent',
        'Development Status :: 1 - Planning',
        'Environment :: Console',
        'Topic :: Text Processing :: Markup :: Markdown',
        'Topic :: Multimedia :: Graphics :: Presentation'
    ],
    install_requires=[
        'pygments==2.15.1',
        'Jinja2==3.1.2',
        'mistletoe==1.0.1',
        'python-dateutil==2.8.2',
        'six==1.16.0',
        'strictyaml==1.7.3'
    ],
    entry_points={
        'console_scripts': [
            'transparencies = transparencies.__main__:main'
        ]
    },
    data_files=(
        list(get_data_files("themes"))
        + list(get_data_files("plugins"))
    ),
    include_package_data=True,
    python_requires='>=3.6'
)
