#!/usr/bin/env python3
import argparse
import os
import strictyaml
import logging
from .build_talk import build
from .searchpaths import get_theme


def dir_path(string):
    if os.path.isdir(string):
        return string
    else:
        raise argparse.ArgumentTypeError(
            f"readable_dir:{string} is not a valid path"
        )


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "head", type=argparse.FileType("r"),
        help="the yaml file containing meta data"
    )
    parser.add_argument(
        "-o", type=dir_path,
        help="folder to store output in"
    )
    parser.add_argument(
        "-l", type=dir_path,
        help="layout directory overriding the default"
    )
    parser.add_argument(
        "-c", type=dir_path,
        help="content directory"
    )
    parser.add_argument(
        "-s", type=dir_path,
        help="static directory"
    )
    parser.add_argument(
        "--log-level",
        type=lambda x: getattr(logging, x),
        help="Configure the logging level."
    )
    parser.add_argument("-v", "--verbose", action="count", default=0)

    args = parser.parse_args()
    if args.log_level:
        logging.basicConfig(level=args.log_level)
    else:
        logging.basicConfig(level=[
            logging.ERROR,
            logging.WARN,
            logging.INFO,
            logging.DEBUG
        ][args.verbose])

    head = strictyaml.load(args.head.read()).data

    if args.c:
        head["content"] = args.c
    elif "content" not in head:
        head["content"] = "content"

    if args.l:
        head["layout"] = args.l
    elif "layout" not in head:
        head["layout"] = "default"

    head["layout"] = get_theme(head["layout"])

    if args.s:
        head["static"] = args.s
    elif "static" not in head:
        head["static"] = "static"

    if args.o:
        head["public"] = args.o
    if "public" not in head:
        head["public"] = "public"

    build(head)


if __name__ == "__main__":
    main()
