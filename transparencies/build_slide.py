import re
import strictyaml
import mistletoe
import logging


def error(msg, *args):
    logger.error(msg, *args)
    raise Exception(msg % args)


logger = logging.getLogger(__name__)


# don't match html lines `<...`
p_double_quotes = re.compile(r"(^<.*)\"(.*?)\"")
p_apostrophe_singular = re.compile(r"(\w)\'s")
p_apostrophe_plural = re.compile(r"(\w)s\'")
p_single_quotes = re.compile(r"\'(.*?)\'")


class renderer(mistletoe.HTMLRenderer):
    def __init__(self, pmanager, *extras):
        logger.info(
            "Add %d tokens: %s",
            len(pmanager.tokens),
            ", ".join(i.__name__ for i in pmanager.tokens)
        )

        extras = list(extras) + pmanager.tokens

        self.code_plugins = pmanager.block_renderers
        for ins in pmanager.token_renderers:
            for name in ins.__dir__():
                if name.startswith("render_"):
                    self.__dict__[name] = getattr(ins, name)
                    logger.info("Add renderer %s", name)

        super().__init__(*extras)

    def render_raw_text(self, token):
        content = token.content

        logger.debug("Apply regex for strings")
        content = re.sub(p_double_quotes, r"\1“\2”", content)
        content = re.sub(p_apostrophe_singular, r"\1’s", content)
        # this substitution will cause the next to fail if it appears inside a
        # quote using single inverted commas
        content = re.sub(p_apostrophe_plural, r"\1s’", content)
        content = re.sub(p_single_quotes, r"‘\1’", content)
        content = (
            content
            # .translate(str.maketrans("'", "’"))
            .replace("---", "—").replace("--", "–")
        )
        return self.escape_html(content)

    def render_document(self, token):
        logger.debug("Call prefix renderers")
        prefix = "\n".join(i.prefix() for i in self.code_plugins)
        logger.debug("Prefix done, %d bytes", len(prefix))

        logger.debug("Call main renderers")
        body = super().render_document(token)
        logger.debug("Body done, %d bytes", len(body))

        logger.debug("Call suffix renderers")
        suffix = "\n".join(i.suffix() for i in self.code_plugins)
        logger.debug("Suffix done, %d bytes", len(suffix))

        return prefix + body + suffix

    def render_block_code(self, token):
        for p in self.code_plugins:
            if p.can_render(token):
                logger.info(
                    "Rendering %s using %s",
                    token.language,
                    p.__class__.__name__
                )
                return p.render_block_code(token)

        logger.info("No renderer for %s", token.language)
        return super().render_block_code(token)


def load_slide(entry, pmanager):
    logger.info("Opening file %s", entry)
    with open(entry, "r") as f:
        raw = f.read()

    if raw[:3] != "---":
        error("Format of file %s unrecognised", entry)

    raw = re.split("^---$", raw, flags=re.M)
    logger.debug(
        "File has %d entries (%d of which are content)",
        len(raw) - 1,
        len(raw) - 2
    )
    metadata = raw[1]

    data = strictyaml.load(metadata).data
    logger.debug("Metdata is %s", str(data))

    data["content"] = []

    for content in raw[2:]:
        with renderer(pmanager) as r:
            data["content"].append(
                r.render(mistletoe.Document(content)).strip()
            )

    if len(data["content"]) == 1:
        data["content"] = data["content"][0]

    return data
