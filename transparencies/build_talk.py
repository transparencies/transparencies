import datetime
import pathlib
import shutil
import jinja2
from .build_slide import load_slide
from .plugins import plugin_manager
import logging

logger = logging.getLogger(__name__)


def error(msg, *args):
    logger.error(msg, *args)
    raise Exception(msg % args)


def build(head, content=None, layout=None, static=None, public=None):
    if content is None:
        head["content"] = content = pathlib.Path(head["content"])
        logger.debug("Using content from head: %s", head["content"])
    elif isinstance(content, str):
        logger.debug("Using content from arg: %s", content)
        head["content"] = content = pathlib.Path(content)

    if layout is None:
        head["layout"] = layout = pathlib.Path(head["layout"])
        logger.debug("Using layout from head: %s", head["layout"])
    elif isinstance(layout, str):
        logger.debug("Using layout from arg: %s", layout)
        head["layout"] = layout = pathlib.Path(layout)

    if static is None:
        head["static"] = static = pathlib.Path(head["static"])
        logger.debug("Using static from head: %s", head["static"])
    elif isinstance(static, str):
        logger.debug("Using static from arg: %s", static)
        head["static"] = static = pathlib.Path(static)

    for directory in (content, layout, static):
        if not directory.is_dir():
            error("Directory %s not found", directory)

    if public is None:
        head["public"] = public = pathlib.Path(head["public"])
        logger.debug("Using public from head: %s", head["public"])
    elif isinstance(public, str):
        logger.debug("Using public from arg: %s", public)
        head["public"] = public = pathlib.Path(public)
    public.mkdir(exist_ok=True)

    env = jinja2.Environment(loader=jinja2.FileSystemLoader(layout))
    logger.debug(
        "Found templates: %s", ", ".join(env.list_templates())
    )

    template_style = env.get_template("style.css.jinja2")
    logger.debug("Use style sheet %s", template_style.filename)

    template = {}
    for templ in layout.iterdir():
        if templ.name[0] == ".":
            logger.debug("Skipping %s, hidded", templ)
            continue
        if templ.suffixes != [".html", ".jinja2"]:
            logger.debug("Skipping %s, not a layout", templ)
            continue

        # TODO This needs improving!
        stem = templ.name.replace(".html.jinja2", "")
        template[stem] = env.get_template(templ.name)
        logger.info(
            "Loading layout %s from %s",
            stem, template[stem].filename
        )

    for file in (layout/"static").iterdir():
        logger.debug("Copy %s -> %s", file, public)
        shutil.copy(file, public)

    for file in static.iterdir():
        logger.debug("Copy %s -> %s", file, public)
        shutil.copy(file, public)

    if "datefmt" not in head:
        head["datefmt"] = "%d %b %Y"

    head["date"] = (
        datetime.datetime.fromisoformat(
            head["date"]
        ).date().strftime(
            head["datefmt"]
        )
    )

    html_style = template_style.render(head)
    with open(public / "style.css", "w") as f:
        f.write(html_style)

    pmanager = plugin_manager(head)

    title = {}
    title["head"] = head
    title["index"] = 0
    title["url"] = "index.html"

    slides = []
    for slide in content.iterdir():
        if slide.name[0] == ".":
            logger.debug("Skipping %s, hidded", templ)
            continue
        if slide.suffix != ".md":
            logger.debug("Skipping %s, not a slide", templ)
            continue
        subs = load_slide(slide, pmanager)
        subs["head"] = head

        if "start" not in subs:
            logger.info("This is the start")
            subs["start"] = False

        subs["url"] = slide.stem + ".html"

        if "next" not in subs:
            subs["next"] = None

        if "layout" not in subs:
            subs["layout"] = "body"

        slides.append(subs)

    title["total"] = len(slides)
    logger.info("We have %d slides", len(slides))

    next_slide = next(x for x in slides if x["start"])
    next_slide["prev_url"] = title["url"]
    title["next"] = next_slide["id"]
    title["next_url"] = next_slide["url"]

    i = 1
    while True:
        next_slide["index"] = i
        i += 1
        next_id = next_slide["next"]
        if next_id is None:
            break
        next_slide = next(x for x in slides if x["id"] == next_id)

    html = template["title"].render(title)
    with open(public / title["url"], "w") as f:
        f.write(html)

    for slide in slides:
        slide["total"] = len(slides)

        if slide["next"] is not None:
            slide["next_url"] = next(
                x["url"] for x in slides if x["id"] == slide["next"]
            )

        if not slide["start"]:
            slide["prev_url"] = next(
                x["url"] for x in slides if x["next"] == slide["id"]
            )

        for action in pmanager.global_actions:
            slide = action.preprocess_slide(slide)
        html = template[slide["layout"]].render(slide)
        for action in pmanager.global_actions:
            subs = action.postprocess_slide(html)

        with open(public / slide["url"], "w") as f:
            f.write(html)
