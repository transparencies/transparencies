# Transparencies

`transparencies` is a python program that compiles slides into a website.
See it in action [here](https://transparencies.gitlab.io/transparencies/)

## Why?
Traditionally, slides follow the limitations set by physical media.
In particular, they have a fixed size and aspect ratio.
`transparencies` is a simpler alternative to something like `reveal.js`.
Metadata is entered in [`yaml`](https://gitlab.com/transparencies/template/-/blob/root/head.yaml) and content is typed up in markdown.
Layout is controlled with `jinja2` templates, while styling is applied from `css`.
The python script generates the html and the browser handles the rest.

While the core is very minimal, we do provide a set of plugins for maths support, tikz, etc.

## Usage
You can install `transparencies` with
```shell
$ pip install git+https://gitlab.com/transparencies/transparencies
```
for local usage.
It is recommended that you fork [our template](https://gitlab.com/transparencies/template) and modify it accordingly.
To run `transparencies` locally, execute
```shell
$ transparencies head.yaml
$ firefox public/index.html
```
We provide a [docker image](https://gitlab.com/transparencies/transparencies/container_registry/2774825) that includes all officially supported plugins as well as a tiny LaTeX installation that you can use in your CI scripts.
For GitLab we provide a [fully functional configuration](https://gitlab.com/transparencies/template/-/blob/root/.gitlab-ci.yml)

### Global metadata
The talks metadata is stored in [`head.yaml`](https://gitlab.com/transparencies/template/-/blob/root/head.yaml).
A minimal example is
```yaml
# Configuration for transparencies talk

# This is the title of your talk. Depending on the theme it
# appears on the title slide and/or on subsequent slides
title: <Your title>

# This is the subtitle on the talk. It would usually only
# appear on the title slide
description: <Your subtitle>

# Your name as the author, appearing on every slide
author: <Your name>

# The date you give the talk formatted as %Y-%m-%d
date: YYYY-MM-DD
```

### Slides
Slides are stored in the `content` folder and follow the following format
```yaml
---
title: <Slide title>
id: <unique id of the slide>
next: <unique id of the next slide>
---
Markdown goes here
```
The header contains at least `title` and a unique `id`.
Most slides will also contain the link to the `next` slide.
The first slide is specified by the key `start: true`.

There is an additional `layout` key.
Which layouts are available depend on the theme used but in general we have
 * `body` (default): a simple slide without any special features
 * `image`: a slide that contains just an image which is specified with the optional `image` key
 * `two-column`: a two-column slide, the boundary is specified with another `---`, ie.
```yaml
---
title: <Slide title>
id: <unique id of the slide>
next: <unique id of the next slide>
layout: two-column
---
Column 1
---
Column 2
```
 * `two-column-image`: a two-column image with a normal image in the right column.
    The image's url is specified with the `image` key

## Plugins
Plugins are written in python and activated in `head.yaml`.
The following paths are searched for plugins to be imported
 * the colon-separated directories in `TRANSPARENCIES_PLUGIN_PATH`
 * `transparencies`' root folder.
   This is either the source directory when running without installing or `$PREFIX/share/transparencies`

### `inlinemaths` and `displaymaths`
A LaTeX package to allow render-time inline and displayed maths.
See [this issue](https://gitlab.com/transparencies/transparencies/-/issues/5) for more information.
Inline maths is indicated with `$` and display maths with a `maths` codeblock.

 * `preamble`: The LaTeX preamble can be specified either as a list of commands or as a path to a LaTeX file
 * `template`: For use cases requiring further customisation, the template file can also be overridden.
 * `args`: A list of command line arguments that are passed to the LaTeX compiler

### `tikz`
A LaTeX package to allow render-time TikZ diagram generation, see above for options.
This defines a `tikz` codeblock that will be rendered

### `syntax`
A syntax highlighter using [Pygments](https://pygments.org/), see the [manual](https://pygments.org/languages/) for supported languages.

 * `style`: the [Pygments style](https://pygments.org/styles/) to use

### `bibliography`
An extensible bibliography generator, currently only supporting [INSPIRE](https://inspirehep.net/) and local entries.
You can cite using `[[citekey]]``.

 * `me`: a list of list identifying you as the author of a paper
```yaml
me:
  - - First Name
    - Last Name
  - - F.
    - Last Name
  - - F.
    - Last Name Variant
```
 * `initial`: your initials will be used instead of your surname
 * `local`: a list of local papers that are identified with `[[local:citekey1]]`.
    `url` and `title` are optional
```yaml
local:
 citekey1:
   authors:
     - Firstname_Lastname
     - Firstname_Lastname2
   year: YYYY
   url: https://website/of/paper
   title: Title of the Paper
 citekey2:
   ...
```

### `animation`
A plugin to show animated figures of images (eg. generated with matplotlib).
This defines the `animation` codeblock that is yaml specifying the animation.
```yaml
loop: true|false # whether to loop once the animation has finished
fps: <number of ticks per second>
# You can specify the frames either with a pattern
pattern: path/to/files%d.svg
nframes: <number of frames>
time:
    0..4: 0    # sets time for frame 0,1,2,3 to 0
    5..=28: 2  # sets time for frame 5,...,28 to 2
    29..: 1    # set all remaining frames to 1
# or an explicit list
list:
    - url: path/to/first/frame.svg
      time: <number of ticks this should be shown for>
    - url: path/to/second/frame.svg
      time: <number of ticks this should be shown for>
    - ...
```
You can vary the speed of the animation over its length by specifying the number of ticks any frame should appear.
Here `0` means manual intervention is required for the next frame to appear.


### `timer`
A simple plugin to track how long your talk is going to be.
This is calculated from the `time` key you have to add to each slide.
